# Tsitsulin YouTube

## Description
Здесь собран код по теме рефакторинга на [моём YouTube канале](https://www.youtube.com/channel/UCsRJwXPaD7BLxU1fo1QEztg).

## Install
- clone the project
- cd [Project]/dev
- run `docker-compose up -d`

## Usage
`docker exec -u developer -it youtube-php-<branch> bash -c "<Your Command>"`
