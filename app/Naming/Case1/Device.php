<?php declare(strict_types=1);

namespace YouTube\Refactoring\Naming\Case1;

use ReflectionClass;
use YouTube\Refactoring\Naming\Case1\Storage\Storage;

/**
 *
 */
final class Device
{
    /**
     * @var Storage
     */
    private Storage $database;
    /**
     * @var int
     */
    private int $id;
    /**
     * @var string
     */
    private string $name;
    /**
     * @var bool
     */
    private bool $battery;
    /**
     * @var int|null
     */
    private ?int $batteryId;
    /**
     * @var bool
     */
    private bool $calculateBatteryLife;
    /**
     * @var int|null
     */
    private ?int $batteryLife;
    /**
     * @var array
     */
    private array $hidden = [
        'hidden',
        'database',
    ];

    /**
     * @param Storage $database
     */
    public function __construct(Storage $database)
    {
        $this->database = $database;
    }

    /**
     * Get the device ID.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the device Name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the device battery flag.
     *
     * @return bool
     */
    public function getBattery(): bool
    {
        return $this->battery;
    }

    /**
     * Get the device Battery ID.
     *
     * @return int
     */
    public function getBatteryId(): int
    {
        return $this->batteryId;
    }

    /**
     * Get "calculate battery life" flag.
     *
     * @return bool
     */
    public function getCalculateBatteryLife(): bool
    {
        return $this->calculateBatteryLife;
    }

    /**
     * Get the device Battery Life.
     *
     * @return int|null
     */
    public function getBatteryLifeInHours(): ?int
    {
        return $this->batteryLife;
    }

    /**
     * Set the device ID.
     *
     * @param int $id
     * @return Device
     */
    public function setId(int $id): Device
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set the device Name.
     *
     * @param string $name
     * @return Device
     */
    public function setName(string $name): Device
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set the device Battery flag.
     *
     * @param bool $battery
     * @return Device
     */
    public function setBattery(bool $battery): Device
    {
        $this->battery = $battery;
        return $this;
    }

    /**
     * Set the device Battery ID.
     *
     * @param int|null $batteryId
     * @return Device
     */
    public function setBatteryId(?int $batteryId): Device
    {
        $this->batteryId = $batteryId;
        return $this;
    }

    /**
     * Set the device "calculate battery life" flag.
     *
     * @param bool $calculateBatteryLife
     * @return Device
     */
    public function setCalculateBatteryLife(bool $calculateBatteryLife): Device
    {
        $this->calculateBatteryLife = $calculateBatteryLife;
        return $this;
    }

    /**
     * Set the device Battery Life.
     *
     * @param int|null $batteryLifeInHours
     * @return $this
     */
    public function setBatteryLife(?int $batteryLifeInHours): Device
    {
        $this->batteryLife = $batteryLifeInHours;
        return $this;
    }

    /**
     * Save the device state.
     */
    public function save(): void
    {
        $this->database->saveDevice($this);
    }

    /**
     * Get array of the device arguments.
     *
     * @return array
     */
    public function getArray(): array
    {
        $array = [];
        $properties = (new ReflectionClass($this))->getProperties();
        foreach ($properties as $property) {
            $propertyName = $property->getName();
            if (in_array($propertyName, $this->hidden)) {
                continue;
            }
            $value = $this->{$propertyName};
            $array[] = $value;
        }

        return $array;
    }
}
