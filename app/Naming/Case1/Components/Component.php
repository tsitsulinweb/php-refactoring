<?php

namespace YouTube\Refactoring\Naming\Case1\Components;

/**
 * Component.
 */
abstract class Component
{
    /**
     * @var IConfig
     */
    private IConfig $config;

    /**
     * @param IConfig $config
     */
    public function __construct(IConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Get config.
     *
     * @return IConfig
     */
    final protected function getConfig(): IConfig
    {
        return $this->config;
    }
}