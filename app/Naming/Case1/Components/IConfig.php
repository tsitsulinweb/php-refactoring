<?php

namespace YouTube\Refactoring\Naming\Case1\Components;

use YouTube\Refactoring\Naming\Case1\Storage\Storage;

/**
 * Config interface.
 */
interface IConfig
{
    /**
     * Get database.
     *
     * @return Storage
     */
    public function getStorage(): Storage;
}