<?php

namespace YouTube\Refactoring\Naming\Case1\Tests;

use YouTube\Refactoring\Naming\Case1\Device;
use YouTube\Refactoring\Naming\Case1\DeviceProvider;
use YouTube\Refactoring\Naming\Case1\DeviceTaskWorker;
use YouTube\Refactoring\Naming\Case1\Tests\Mocking\BaseTestCase;

/**
 * Device task worker test.
 */
class DeviceTaskWorkerTest extends BaseTestCase
{
    /**
     * @covers DeviceTaskWorker::calculateBatteryLife()
     */
    public function testCalculateBatteryLife()
    {
        $config = $this->getConfig();

        // Calculating the battery life.
        (new DeviceTaskWorker($config))->calculateBatteryLife();
        $calculate = function (int $id, int $batteryId) {
            return round((1000 + $batteryId * 10) / ($id * 20));
        };

        $this->assertEquals(
            $expected = [
                [1, $calculate(1, 1)],
                [2, $calculate(2, 1)],
                [3, $calculate(3, 3)],
                [4, null],
                [5, $calculate(5, 5)],
                [6, null],
            ],
            $actual = array_map(function (Device $device) {
                return [$device->getId(), $device->getBatteryLifeInHours()];
            }, (new DeviceProvider($config))->getAll()),
            $this->getDetails($expected, $actual)
        );
    }
}