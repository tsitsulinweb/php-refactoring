<?php declare(strict_types=1);

namespace YouTube\Refactoring\Naming\Case1\Tests;

use YouTube\Refactoring\Naming\Case1\Device;
use YouTube\Refactoring\Naming\Case1\DeviceProvider;
use YouTube\Refactoring\Naming\Case1\Tests\Mocking\BaseTestCase;

/**
 * Device provider test.
 */
final class DeviceProviderTest extends BaseTestCase
{
    /**
     * @covers DeviceProvider::getAll()
     */
    public function testGetAll(): void
    {
        $config = $this->getConfig();

        $this->assertEquals(
            $expected = [
                [1, true, null],
                [2, true, null],
                [3, true, null],
                [4, false, null],
                [5, true, null],
                [6, false, null],
            ],
            $actual = array_map(function (Device $device) {
                return [
                    $device->getId(),
                    $device->getBattery(),
                    $device->getBatteryLifeInHours(),
                ];
            }, (new DeviceProvider($config))->getAll()),
            $this->getDetails($expected, $actual)
        );
    }

    /**
     * @covers DeviceProvider::getAllWithBattery()
     */
    public function testGetAllWithBattery(): void
    {
        $config = $this->getConfig();

        $this->assertEquals(
            $expected = [
                [1, true, null],
                [2, true, null],
                [3, true, null],
                [5, true, null],
            ],
            $actual = array_map(function (Device $device) {
                return [
                    $device->getId(),
                    $device->getBattery(),
                    $device->getBatteryLifeInHours(),
                ];
            }, (new DeviceProvider($config))->getAllWithBattery()),
            $this->getDetails($expected, $actual)
        );
    }
}
