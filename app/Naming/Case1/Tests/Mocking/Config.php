<?php

namespace YouTube\Refactoring\Naming\Case1\Tests\Mocking;

use YouTube\Refactoring\Naming\Case1\Components\IConfig;
use YouTube\Refactoring\Naming\Case1\Storage\Storage;

/**
 * Mock Config.
 */
class Config implements IConfig
{
    private Storage $storage;

    /**
     * {@inheritDoc}
     */
    public function getStorage(): Storage
    {
        return $this->storage
            ?? $this->storage = new Storage(new Orm());
    }
}