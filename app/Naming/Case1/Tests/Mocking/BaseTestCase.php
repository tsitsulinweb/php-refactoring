<?php

namespace YouTube\Refactoring\Naming\Case1\Tests\Mocking;

use PHPUnit\Framework\TestCase;
use YouTube\Refactoring\Naming\Case1\Components\IConfig;

/**
 * Base Test Case.
 */
abstract class BaseTestCase extends TestCase
{
    /**
     * @var IConfig
     */
    private IConfig $config;

    /**
     * Get config.
     *
     * @return IConfig
     */
    final protected function getConfig(): IConfig
    {
        return $this->config ??= $this->config = new Config();
    }

    /**
     * Get Details.
     *
     * @param array $expected
     * @param array $actual
     * @return string
     */
    final protected function getDetails(array $expected, array $actual): string
    {
        return json_encode([$expected, $actual], JSON_PRETTY_PRINT);
    }
}