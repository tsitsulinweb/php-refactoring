<?php

namespace YouTube\Refactoring\Naming\Case1\Tests\Mocking;

use YouTube\Refactoring\Naming\Case1\Storage\IOrm;

/**
 * Mock ORM.
 */
class Orm implements IOrm
{
    /**
     * @return array
     */
    public function getDevices(): array
    {
        return json_decode(
            file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'fixtures.json'),
            true
        )['devices'];
    }
}