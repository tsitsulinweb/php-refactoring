<?php declare(strict_types=1);

namespace YouTube\Refactoring\Naming\Case1;

use YouTube\Refactoring\Naming\Case1\Components\Component;

/**
 * Device Task Worker.
 */
final class DeviceTaskWorker extends Component
{
    /**
     * Calculate a battery life.
     */
    public function calculateBatteryLife()
    {
        foreach ((new DeviceProvider($this->getConfig()))->getAll() as $device) {
            if ($device->getCalculateBatteryLife()) {
                // Some logic for calculating battery life.
                if (true) {
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    // 1000 lines of code.
                    if (true) {
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        // 1000 lines of code.
                        $batteryCapacity = 1000 + $device->getBatteryId() * 10;
                        $devicePower = $device->getId() * 20;
                        $batteryLife = (int)round($batteryCapacity / $devicePower);
                        $device->setBatteryLife($batteryLife);
                        $device->save();
                    }
                }
            }
        }
    }
}