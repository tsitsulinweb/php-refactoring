<?php declare(strict_types=1);

namespace YouTube\Refactoring\Naming\Case1;

use YouTube\Refactoring\Naming\Case1\Components\Component;

/**
 * Device Provider.
 */
final class DeviceProvider extends Component
{
    /**
     * Get devices.
     * @return Device[]
     */
    public function getAll(): array
    {
        return $this
            ->getConfig()
            ->getStorage()
            ->getDevices();
    }

    /**
     * Get devices with a battery.
     * @return Device[]
     */
    public function getAllWithBattery(): array
    {
        return array_values(array_filter(
            array_map(
                function (Device $device) {
                    return $device->getBattery()
                        ? $device
                        : null;
                },
                $this->getConfig()->getStorage()->getDevices()
            ),
            function (?Device $device) {
                return $device instanceof Device;
            }
        ));
    }
}