<?php

namespace YouTube\Refactoring\Naming\Case1\Storage;

use YouTube\Refactoring\Naming\Case1\Device;

/**
 * ORM interface.
 */
interface IOrm
{
    /**
     * @return Device[]
     */
    public function getDevices(): array;
}