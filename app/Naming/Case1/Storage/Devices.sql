CREATE TABLE devices
(
    id                     INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name                   VARCHAR(100) NOT NULL,
    battery                BOOLEAN      NOT NULL,
    battery_id             INT          NOT NULL,
    calculate_battery_life BOOLEAN      NOT NULL,
    battery_life           SMALLINT DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;