<?php declare(strict_types=1);

namespace YouTube\Refactoring\Naming\Case1\Storage;

use YouTube\Refactoring\Naming\Case1\Device;

/**
 * Database.
 */
final class Storage
{
    /**
     * @var array[]
     */
    private array $devices;

    /**
     * @param IOrm $orm
     */
    public function __construct(IOrm $orm)
    {
        $this->devices = $orm->getDevices();
    }

    /**
     * Get devices.
     *
     * @return Device[]
     */
    public function getDevices(): array
    {
        return array_map(function (array $row) {
            [$id, $name, $battery, $batteryId, $calculateBatteryLife, $batteryLife] = $row;
            return (new Device($this))
                ->setId($id)
                ->setName($name)
                ->setBattery($battery)
                ->setBatteryId($batteryId)
                ->setCalculateBatteryLife($calculateBatteryLife)
                ->setBatteryLife($batteryLife);
        }, $this->devices);
    }

    /**
     * Save a device state.
     *
     * @param Device $device
     */
    public function saveDevice(Device $device)
    {
        $updatedDevices = [];
        foreach ($this->devices as $deviceRow) {
            [$id] = [0];
            $updatedDevices[] = $device->getId() === $deviceRow[$id]
                ? $device->getArray()
                : $deviceRow;
        }
        $this->devices = $updatedDevices;
    }
}