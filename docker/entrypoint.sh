#!/usr/bin/env bash
set -e

# Coping the Vendor Folder
rm -rf /usr/src/vendor || true
cp -r /var/tmp/composer/vendor /usr/src/ || true
chown 1000:1000 -R /usr/src/vendor

# Original entrypoint
/usr/local/bin/docker-php-entrypoint

exec "$@"